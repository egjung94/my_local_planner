/*
 *  Copyright (c) 2018, Nagoya University
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 *  * Neither the name of Autoware nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 *  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 *  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************/

// headers in local directory
//#include "object_map/object_map_utils.hpp"
#include "costmap_generator/costmap_generator.h"

// Constructor
CostmapGenerator::CostmapGenerator()
  : private_nh_("~")
  , has_subscribed_wayarea_(false)
  , OBJECTS_BOX_COSTMAP_LAYER_("objects_box")
  , OBJECTS_CONVEX_HULL_COSTMAP_LAYER_("objects_convex_hull")
  , SENSOR_POINTS_COSTMAP_LAYER_("sensor_points")
  , VECTORMAP_COSTMAP_LAYER_("vectormap")
  , COMBINED_COSTMAP_LAYER_("costmap")
{
}

CostmapGenerator::~CostmapGenerator()
{
}

void CostmapGenerator::init()
{
  private_nh_.param<std::string>("lidar_frame", lidar_frame_, "velodyne");
  private_nh_.param<std::string>("map_frame", map_frame_, "map");
  private_nh_.param<double>("grid_min_value", grid_min_value_, 0.0);
  private_nh_.param<double>("grid_max_value", grid_max_value_, 1.0);
  private_nh_.param<double>("grid_resolution", grid_resolution_, 1.0);
  private_nh_.param<double>("grid_length_x", grid_length_x_, 50);
  private_nh_.param<double>("grid_length_y", grid_length_y_, 30);
  private_nh_.param<double>("grid_position_x", grid_position_x_, 0);
  private_nh_.param<double>("grid_position_y", grid_position_y_, 0);
  private_nh_.param<double>("maximum_lidar_height_thres", maximum_lidar_height_thres_, 0.3);
  private_nh_.param<double>("minimum_lidar_height_thres", minimum_lidar_height_thres_, -2.2);
  private_nh_.param<bool>("use_objects_box", use_objects_box_, false);
  private_nh_.param<bool>("use_objects_convex_hull", use_objects_convex_hull_, true);
  private_nh_.param<bool>("use_points", use_points_, true);
  private_nh_.param<bool>("use_wayarea", use_wayarea_, true);
  private_nh_.param<double>("expand_polygon_size", expand_polygon_size_, 1.0);
  private_nh_.param<int>("size_of_expansion_kernel", size_of_expansion_kernel_, 9);
  private_nh_.param<int>("size_of_expansion_kernel", size_of_expansion_kernel_, 9);
  private_nh_.param<std::string>("points_input", points_input_, "/points_no_groud");

  initGridmap();
}

void CostmapGenerator::run()
{
  // pub_costmap_ = nh_.advertise<grid_map_msgs::GridMap>("gridmap", 1);
  pub_gridmap_left_ = nh_.advertise<grid_map_msgs::GridMap>("gridmap_left", 10);
  pub_gridmap_right_ = nh_.advertise<grid_map_msgs::GridMap>("gridmap_right", 10);
  pub_costmap_ = nh_.advertise<nav_msgs::OccupancyGrid>("costmap", 1);

  if (use_points_)
  {
    sub_left_points_ = nh_.subscribe("/velodyne_left/velodyne_points", 1, &CostmapGenerator::leftPointCloudCallback, this);
    sub_right_points_ = nh_.subscribe("/velodyne_right/velodyne_points", 1, &CostmapGenerator::rightPointCloudCallback, this);
    sub_ouster_points_ = nh_.subscribe(points_input_, 1, &CostmapGenerator::pointCloudCallback, this);
  }

  grid_map_left_sub_.subscribe(nh_, "gridmap_left", 10);
  grid_map_right_sub_.subscribe(nh_, "gridmap_right", 10);
  
  sync.reset(new Sync(my_sync_policy(1000), grid_map_left_sub_, grid_map_right_sub_));
  sync->registerCallback(boost::bind(&CostmapGenerator::gridMapCallback, this, _1, _2));
}


void CostmapGenerator::gridMapCallback(const grid_map_msgs::GridMapConstPtr& left, const grid_map_msgs::GridMapConstPtr& right)
{
  grid_map::GridMap costmap_left;
  grid_map::GridMap costmap_right;
  grid_map::GridMap costmap_combined;

  grid_map::GridMapRosConverter::fromMessage(*left, costmap_left);
  grid_map::GridMapRosConverter::fromMessage(*right, costmap_right);
  
  costmap_combined = combinePointCloudCostmap(costmap_left, costmap_right);

  std_msgs::Header in_header;
  in_header.frame_id = "base_footprint";
  in_header.stamp = left->info.header.stamp;

  publishOccupancyGridMsg(costmap_combined, in_header);
}

void CostmapGenerator::rightPointCloudCallback(const sensor_msgs::PointCloud2 msg){
  pcl::PointCloud<pcl::PointXYZ>::Ptr in_sensor_points(new pcl::PointCloud<pcl::PointXYZ>);
  pcl::PointCloud<pcl::PointXYZ>::Ptr transformed_sensor_points(new pcl::PointCloud<pcl::PointXYZ>);
  pcl::fromROSMsg(msg, *in_sensor_points);

  /* lookup Transform from "/velodyne_right/velodyne_point" to "base_link" */
  tf::StampedTransform stf;

  try
  {
    tf_listener_.lookupTransform("base_footprint", msg.header.frame_id, ros::Time(0), stf);
  }
  catch (tf::TransformException ex)
  {
    ROS_ERROR("%s", ex.what());
  }

  Eigen::Affine3d matrix;
  tf::Transform tf = stf;
  tf::transformTFToEigen(tf, matrix);
  pcl::transformPointCloud(*in_sensor_points, *transformed_sensor_points, matrix);

  costmap_[RIGHT_SENSOR_POINTS_COSTMAP_LAYER_] = generateSensorPointsCostmap(transformed_sensor_points);
  
  std_msgs::Header in_header;
  in_header.frame_id = "base_footprint";
  in_header.stamp = msg.header.stamp;

  publishGridMapMsg(costmap_, in_header, pub_gridmap_right_);
}

void CostmapGenerator::leftPointCloudCallback(const sensor_msgs::PointCloud2 msg){
  pcl::PointCloud<pcl::PointXYZ>::Ptr in_sensor_points(new pcl::PointCloud<pcl::PointXYZ>);
  pcl::PointCloud<pcl::PointXYZ>::Ptr transformed_sensor_points(new pcl::PointCloud<pcl::PointXYZ>);
  pcl::fromROSMsg(msg, *in_sensor_points);

  /* lookup Transform from "/velodyne_left/velodyne_point" to "base_link" */
  tf::StampedTransform stf;

  try
  {
    tf_listener_.lookupTransform("base_footprint", msg.header.frame_id, ros::Time(0), stf);
  }
  catch (tf::TransformException ex)
  {
    ROS_ERROR("%s", ex.what());
  }

  Eigen::Affine3d matrix;
  tf::Transform tf = stf;
  tf::transformTFToEigen(tf, matrix);
  pcl::transformPointCloud(*in_sensor_points, *transformed_sensor_points, matrix);

  costmap_[LEFT_SENSOR_POINTS_COSTMAP_LAYER_] = generateSensorPointsCostmap(transformed_sensor_points);
  
  std_msgs::Header in_header;
  in_header.frame_id = "base_footprint";
  in_header.stamp = msg.header.stamp;

  publishGridMapMsg(costmap_, in_header, pub_gridmap_left_);
}

void CostmapGenerator::pointCloudCallback(const sensor_msgs::PointCloud2 msg){
  pcl::PointCloud<pcl::PointXYZ>::Ptr in_sensor_points(new pcl::PointCloud<pcl::PointXYZ>);
  pcl::PointCloud<pcl::PointXYZ>::Ptr transformed_sensor_points(new pcl::PointCloud<pcl::PointXYZ>);
  pcl::fromROSMsg(msg, *in_sensor_points);

  /* lookup Transform from "os1_lidar" to "base_link" */
  tf::StampedTransform stf;

  try
  {
    tf_listener_.lookupTransform("base_footprint", msg.header.frame_id, ros::Time(0), stf);
  }
  catch (tf::TransformException ex)
  {
    ROS_ERROR("%s", ex.what());
  }

  Eigen::Affine3d matrix;
  tf::Transform tf = stf;
  tf::transformTFToEigen(tf, matrix);
  pcl::transformPointCloud(*in_sensor_points, *transformed_sensor_points, matrix);

  costmap_[SENSOR_POINTS_COSTMAP_LAYER_] = generateSensorPointsCostmap(transformed_sensor_points);
  
  std_msgs::Header in_header;
  in_header.frame_id = "base_footprint";
  in_header.stamp = msg.header.stamp;

  publishOccupancyGridMsg(costmap_, in_header);
}


void CostmapGenerator::initGridmap()
{
  costmap_.setFrameId(lidar_frame_);
  costmap_.setGeometry(grid_map::Length(grid_length_x_, grid_length_y_), grid_resolution_,
                       grid_map::Position(grid_position_x_, grid_position_y_));

  costmap_.add(SENSOR_POINTS_COSTMAP_LAYER_, grid_min_value_);
  costmap_.add(RIGHT_SENSOR_POINTS_COSTMAP_LAYER_, grid_min_value_);
  costmap_.add(LEFT_SENSOR_POINTS_COSTMAP_LAYER_, grid_min_value_);
  costmap_.add(OBJECTS_BOX_COSTMAP_LAYER_, grid_min_value_);
  costmap_.add(OBJECTS_CONVEX_HULL_COSTMAP_LAYER_, grid_min_value_);
  costmap_.add(VECTORMAP_COSTMAP_LAYER_, grid_min_value_);
  costmap_.add(COMBINED_COSTMAP_LAYER_, grid_min_value_);
}

grid_map::Matrix
CostmapGenerator::generateSensorPointsCostmap(const pcl::PointCloud<pcl::PointXYZ>::Ptr& in_sensor_points)
{
  grid_map::Matrix sensor_points_costmap = points2costmap_.makeCostmapFromSensorPoints(
      maximum_lidar_height_thres_, minimum_lidar_height_thres_, grid_min_value_, grid_max_value_, costmap_,
      SENSOR_POINTS_COSTMAP_LAYER_, in_sensor_points);
  return sensor_points_costmap;
}

grid_map::GridMap
CostmapGenerator::combinePointCloudCostmap(grid_map::GridMap& costmap_left, grid_map::GridMap& costmap_right)
{
  grid_map::GridMap costmap = costmap_left;

  for (int i = 0; i < costmap.getSize()(0); i++)
  {
    for (int j = 0; j < costmap.getSize()(1); j++)
    {
      float left_cost = costmap_left.at(LEFT_SENSOR_POINTS_COSTMAP_LAYER_, grid_map::Index(i, j));
      float right_cost = costmap_right.at(RIGHT_SENSOR_POINTS_COSTMAP_LAYER_, grid_map::Index(i, j));
      if (left_cost == 1 || right_cost == 1){
        costmap[SENSOR_POINTS_COSTMAP_LAYER_](i, j) = 1;
      }
    }
  }
  return costmap;
}

void CostmapGenerator::publishOccupancyGridMsg(const grid_map::GridMap& costmap, const std_msgs::Header& in_header)
{
  nav_msgs::OccupancyGrid out_occupancy_grid;
  grid_map::GridMapRosConverter::toOccupancyGrid(costmap, SENSOR_POINTS_COSTMAP_LAYER_, grid_min_value_, grid_max_value_,
                                                 out_occupancy_grid);
  out_occupancy_grid.header = in_header;
  pub_costmap_.publish(out_occupancy_grid);
}

void CostmapGenerator::publishGridMapMsg(const grid_map::GridMap& costmap, const std_msgs::Header& in_header, ros::Publisher pub)
{
  grid_map_msgs::GridMap out_gridmap_msg;
  grid_map::GridMapRosConverter::toMessage(costmap, out_gridmap_msg);
  out_gridmap_msg.info.header = in_header;
  pub.publish(out_gridmap_msg);  
}