#!/usr/bin/env python
import rospy
import os
import tf
from rospy_message_converter import json_message_converter
from nav_msgs.msg import Path
from geometry_msgs.msg import PoseStamped
from custom_msgs.msg import *

def publishPath():
	path_pub = rospy.Publisher('path', Path, queue_size=10, latch=True)
	waypoint_pub = rospy.Publisher('base_waypoints', Lane, queue_size=10, latch=True)
	rospy.init_node('path_publisher', anonymous=False)
	rate = rospy.Rate(10)
	
	script_dir = os.path.dirname(__file__)
	rel_path = "global_path.txt"
	abs_file_path = os.path.join(script_dir, rel_path)
	#"/home/suninder/catkin_ws/src/my_local_planner/utils/rospy_message_converter/src/rospy_message_converter/global_path.txt"
	f = open(abs_file_path, "r")
	json_str = f.read()
	f.close()

	# Path
	path = json_message_converter.convert_json_to_ros_message('nav_msgs/Path', json_str)
	path.header.stamp = rospy.Time(secs=1584420952, nsecs=997440100)
	# Waypoints
	lane_msg = Lane()
	lane_msg.header = path.header
	lane_msg.increment = 1
	lane_msg.lane_id = 0
	lane_msg.lane_index = 0
	lane_msg.cost = 0
	lane_msg.closest_object_distance = 0
	lane_msg.closest_object_velocity = 0
	lane_msg.is_blocked = False

	for i in range(len(path.poses)):

		# modify path timestamp
		path.poses[i].header.stamp = rospy.Time(secs=1584420952, nsecs=997440100)

		wp = Waypoint()
		wp.pose = path.poses[i]
		wp.twist.twist.linear.x = 1
		wp.lane_id = 0
		wp.stop_line_id = 0 
		wp.left_lane_id = 0 
		wp.right_lane_id = 0 
		wp.time_cost = 0 
		wp.gid = i 
		wp.cost = 0
		lane_msg.waypoints.append(wp)

  	print("base_waypoints size: " + str(len(lane_msg.waypoints)))


	while not rospy.is_shutdown():
		# base_path_pub.publish(base_path)
		path_pub.publish(path)
		waypoint_pub.publish(lane_msg)
		rate.sleep()

if __name__ == '__main__':

	try:
		publishPath()
	except rospy.ROSInterruptException:
		pass
