#!/usr/bin/env python
import rospy
from rospy_message_converter import json_message_converter
from nav_msgs.msg import Path

def pathCallback(msg):
	json_str = json_message_converter.convert_ros_message_to_json(msg)
	f = open("global_path.txt", "w+")
	f.write(json_str)
	f.close()

def basePathCallback(msg):
	json_str = json_message_converter.convert_ros_message_to_json(msg)
	f = open("base_path.txt", "w+")
	f.write(json_str)
	f.close()

def convertToJson():
	rospy.init_node('path_listener', anonymous=True)
	rospy.Subscriber("path", Path, pathCallback)
	rospy.Subscriber("base_path", Path, basePathCallback) 
	rospy.spin()

if __name__ == '__main__':
	try:
		convertToJson()
	except rospy.ROSInterruptException:
		pass