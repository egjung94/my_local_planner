/*
 * Copyright 2015-2019 Autoware Foundation. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "waypoint_planner/astar_avoid/astar_avoid.h"

//ros::Publisher current_pose_pub_ = nh_.advertise<geometry_msgs::PoseStamped>("current_pose", 1);
//ros::Publisher closest_waypoint_pub_ = nh_.advertise<std_msgs::Int32>("closest_waypoint", 1);

AstarAvoid::AstarAvoid()
  : nh_()
  , private_nh_("~")
  , closest_waypoint_index_(-1)
  , obstacle_waypoint_index_(-1)
  , closest_local_index_(-1)
  , costmap_initialized_(false)
  , current_pose_initialized_(false)
  , current_velocity_initialized_(false)
  , base_waypoints_initialized_(false)
  , closest_waypoint_initialized_(false)
  , terminate_thread_(false)
{
  private_nh_.param<int>("safety_waypoints_size", safety_waypoints_size_, 100);
  private_nh_.param<double>("update_rate", update_rate_, 10.0);
  private_nh_.param<bool>("enable_avoidance", enable_avoidance_, true);
  private_nh_.param<double>("avoid_waypoints_velocity", avoid_waypoints_velocity_, 10.0);
  private_nh_.param<double>("avoid_start_velocity", avoid_start_velocity_, 5.0);
  private_nh_.param<double>("replan_interval", replan_interval_, 0.2);
  private_nh_.param<int>("search_waypoints_size", search_waypoints_size_, 50);
  private_nh_.param<int>("search_waypoints_delta", search_waypoints_delta_, 2);
  private_nh_.param<int>("closest_search_size", closest_search_size_, 30);

  // A* search param
  // base configs
  private_nh_.param<bool>("use_back", use_back_, false);
  private_nh_.param<bool>("use_potential_heuristic", use_potential_heuristic_, false);
  private_nh_.param<bool>("use_wavefront_heuristic", use_wavefront_heuristic_, true);
  private_nh_.param<double>("time_limit", time_limit_, 5000.0);

  // robot configs
  private_nh_.param<double>("robot_length", robot_length_, 0.05);
  private_nh_.param<double>("robot_width", robot_width_, 0.05);
  private_nh_.param<double>("robot_base2back", robot_base2back_, 0.025);
  private_nh_.param<double>("minimum_turning_radius", minimum_turning_radius_, 6.0);

  // search configs
  private_nh_.param<int>("theta_size", theta_size_, 48);
  private_nh_.param<double>("angle_goal_range", angle_goal_range_, 6.0);
  private_nh_.param<double>("curve_weight", curve_weight_, 1.2);
  private_nh_.param<double>("reverse_weight", reverse_weight_, 2.00);
  private_nh_.param<double>("lateral_goal_range", lateral_goal_range_, 0.5);
  private_nh_.param<double>("longitudinal_goal_range", longitudinal_goal_range_, 2.0);

  // costmap configs
  private_nh_.param<int>("obstacle_threshold", obstacle_threshold_, 100);
  private_nh_.param<double>("potential_weight", potential_weight_, 10.0);
  private_nh_.param<double>("distance_heuristic_weight", distance_heuristic_weight_, 1.0);


  // Pub and Sub
  safety_waypoints_pub_ = nh_.advertise<custom_msgs::Lane>("safety_waypoints", 1, true);
  // Rviz visualization pub
  local_path_pub_ = nh_.advertise<nav_msgs::Path>("local_path", 1, true);
  current_pose_pub_ = nh_.advertise<geometry_msgs::PoseStamped>("current_pose", 1000);
  // Test
  closest_waypoint_pub_ = nh_.advertise<std_msgs::Int32>("closest_waypoint", 1);
  current_velocity_pub_ = nh_.advertise<geometry_msgs::TwistStamped>("current_velocity", 1, true);
  localizer_pose_pub_ = nh_.advertise<geometry_msgs::PoseStamped>("localizer_pose", 1); // It should be published from lidar library

  costmap_sub_ = nh_.subscribe("costmap", 1, &AstarAvoid::costmapCallback, this);
  //costmap_sub_ = nh_.subscribe("world", 1, &AstarAvoid::globalMapCallback, this);
  current_odom_sub_ = nh_.subscribe("/gps_odom", 1, &AstarAvoid::currentOdomCallback, this);
  current_velocity_sub_ = nh_.subscribe("current_velocity", 1, &AstarAvoid::currentVelocityCallback, this);
  base_waypoints_sub_ = nh_.subscribe("base_waypoints", 1, &AstarAvoid::baseWaypointsCallback, this);
  closest_waypoint_sub_ = nh_.subscribe("closest_waypoint", 1, &AstarAvoid::closestWaypointCallback, this);
  obstacle_waypoint_sub_ = nh_.subscribe("obstacle_waypoint", 1, &AstarAvoid::obstacleWaypointCallback, this);

  rate_ = new ros::Rate(update_rate_);
  br_ = new tf::TransformBroadcaster();
}

AstarAvoid::~AstarAvoid()
{
  publish_thread_.join();
  // delete new?
}

void AstarAvoid::costmapCallback(const nav_msgs::OccupancyGrid& msg)
{
  costmap_ = msg;
  tf::poseMsgToTF(costmap_.info.origin, local2costmap_);
  costmap_initialized_ = true;
}

void AstarAvoid::currentOdomCallback(const nav_msgs::Odometry& odom_msg)
{

  current_pose_global_.pose = odom_msg.pose.pose;
  current_pose_global_.header.frame_id = "base_link";
  current_pose_global_.header.stamp = odom_msg.header.stamp;

  /* modification just for test */
  current_pose_global_.pose.position.x = 353385.7602862002;
  current_pose_global_.pose.position.y =  4026127.8288240302;
  current_pose_global_.pose.orientation.z = 0.25839123311436346;
  current_pose_global_.pose.orientation.w = 0.9660403566361183;

  tf::Transform transform;

  transform.setOrigin(tf::Vector3(current_pose_global_.pose.position.x, 
                                  current_pose_global_.pose.position.y, 
                                  0.0));
  transform.setRotation(tf::Quaternion(current_pose_global_.pose.orientation.x,
                                      current_pose_global_.pose.orientation.y,
                                      current_pose_global_.pose.orientation.z,
                                      current_pose_global_.pose.orientation.w
                                      ));
  
  br_->sendTransform(tf::StampedTransform(transform, odom_msg.header.stamp, "base_link", "base_footprint"));

  if (!enable_avoidance_)
  {
    current_pose_initialized_ = true;
  }
  else
  {
    // frame "world" --> frame "odom"
    // current_pose_local_.pose = transformPose(
    //     current_pose_global_.pose, getTransform(costmap_.header.frame_id, current_pose_global_.header.frame_id));
    current_pose_local_.header.frame_id = costmap_.header.frame_id;
    current_pose_local_.header.stamp = current_pose_global_.header.stamp;

    // TEMP
    current_pose_local_.pose.position.x = 0.0;
    current_pose_local_.pose.position.y = 0.0;
    current_pose_local_.pose.position.z = 0.0;
    current_pose_local_.pose.orientation.z = 0.0;
    current_pose_local_.pose.orientation.w = 1.0;


    // Test
    closest_waypoint_index_ = getLocalClosestWaypoint(base_waypoints_, current_pose_global_.pose, closest_search_size_);
    std_msgs::Int32 closest_waypoint;
    closest_waypoint.data = closest_waypoint_index_;
    closest_waypoint_pub_.publish(closest_waypoint);
    current_pose_initialized_ = true;

    current_velocity_.twist.linear.x = 1.1;
    current_velocity_pub_.publish(current_velocity_);
  }

  // Publish localizer pose
  localizer_pose_pub_.publish(current_pose_global_);

  current_pose_pub_.publish(current_pose_global_);
}

void AstarAvoid::currentVelocityCallback(const geometry_msgs::TwistStamped& msg)
{
  current_velocity_ = msg;
  current_velocity_initialized_ = true;
}

void AstarAvoid::baseWaypointsCallback(const custom_msgs::Lane& msg)
{
  static custom_msgs::Lane prev_base_waypoints;
  base_waypoints_ = msg;

  if (base_waypoints_initialized_)
  {
    // detect waypoint change by timestamp update
    ros::Time t1 = prev_base_waypoints.header.stamp;
    ros::Time t2 = base_waypoints_.header.stamp;
    if ((t2 - t1).toSec() > 1e-3)
    {
      ROS_INFO("Receive new /base_waypoints, reset waypoint index.");
      closest_local_index_ = -1; // reset local closest waypoint
      prev_base_waypoints = base_waypoints_;
    }
  }
  else
  {
    prev_base_waypoints = base_waypoints_;
  }

  base_waypoints_initialized_ = true;
}

void AstarAvoid::closestWaypointCallback(const std_msgs::Int32& msg)
{
  closest_waypoint_index_ = msg.data;

  if (closest_waypoint_index_ == -1)
  {
    closest_local_index_ = -1; // reset local closest waypoint
  }

  closest_waypoint_initialized_ = true;
}

void AstarAvoid::obstacleWaypointCallback(const std_msgs::Int32& msg)
{
  obstacle_waypoint_index_ = msg.data;
}

void AstarAvoid::run()
{

  state_ = AstarAvoid::STATE::INITIALIZING;

  while (ros::ok())
  {
    ros::spinOnce();
    if (checkInitialized())
    {
      break;
    }
    // ROS_WARN("Waiting for subscribing topics...");
    //ros::Duration(1.0).sleep();
  }

  // main loop
  int end_of_avoid_index = -1;
  ros::WallTime start_plan_time = ros::WallTime::now();
  ros::WallTime start_avoid_time = ros::WallTime::now();

  // reset obstacle index
  obstacle_waypoint_index_ = -1;

  // relaying mode at startup
  state_ = AstarAvoid::STATE::RELAYING;

  // start publish thread
  publish_thread_ = std::thread(&AstarAvoid::publishWaypoints, this);

  while (ros::ok())
  {
    ros::spinOnce();

    // relay mode
    if (!enable_avoidance_)
    {
      rate_->sleep();
      continue;
    }

    // avoidance mode
    bool found_obstacle = (obstacle_waypoint_index_ >= 0);

    bool avoid_velocity = (current_velocity_.twist.linear.x < avoid_start_velocity_ / 3.6);

    if (found_obstacle){
      ROS_INFO("Obstacle found");
    }

    // update state
    if (state_ == AstarAvoid::STATE::RELAYING)
    {
      avoid_waypoints_ = base_waypoints_;

      if (found_obstacle)
      {
        ROS_INFO("RELAYING -> STOPPING, Decelerate for stopping");
        state_ = AstarAvoid::STATE::STOPPING;
      }
    }
    else if (state_ == AstarAvoid::STATE::STOPPING)
    {
      bool replan = ((ros::WallTime::now() - start_plan_time).toSec() > replan_interval_);
      
      if (!found_obstacle)
      {
        ROS_INFO("STOPPING -> RELAYING, Obstacle disappers");
        state_ = AstarAvoid::STATE::RELAYING;
      }
      else if (replan && avoid_velocity)
      {
        ROS_INFO("STOPPING -> PLANNING, Start A* planning");

        state_ = AstarAvoid::STATE::PLANNING;
      }
    }
    else if (state_ == AstarAvoid::STATE::PLANNING)
    {
      start_plan_time = ros::WallTime::now();

      if (planAvoidWaypoints(end_of_avoid_index))
      {
        ROS_INFO("PLANNING -> AVOIDING, Found path");
        state_ = AstarAvoid::STATE::AVOIDING;
        start_avoid_time = ros::WallTime::now();
      }
      else
      {
        ROS_INFO("PLANNING -> STOPPING, Cannot find path");
        state_ = AstarAvoid::STATE::STOPPING;
      }
    }
    else if (state_ == AstarAvoid::STATE::AVOIDING)
    {
      bool reached = (getLocalClosestWaypoint(avoid_waypoints_, current_pose_global_.pose, closest_search_size_) > end_of_avoid_index);
      if (reached)
      {
        ROS_INFO("AVOIDING -> RELAYING, Reached goal");
        state_ = AstarAvoid::STATE::RELAYING;
      }
      else if (found_obstacle && avoid_velocity)
      {
        bool replan = ((ros::WallTime::now() - start_avoid_time).toSec() > replan_interval_);
        if (replan)
        {
          ROS_INFO("AVOIDING -> STOPPING, Abort avoiding");
          state_ = AstarAvoid::STATE::STOPPING;
        }
      }
    }

    rate_->sleep();
  }

  terminate_thread_ = true;
}

bool AstarAvoid::checkInitialized()
{
  bool initialized = false;

  // check for relay mode
  initialized = (current_pose_initialized_ && closest_waypoint_initialized_ && base_waypoints_initialized_ &&
                 (closest_waypoint_index_ >= 0));

  // check for avoidance mode, additionally
  if (enable_avoidance_)
  {
    // ROS_INFO("initiailized: %d, %d, %d", initialized, current_velocity_initialized_, costmap_initialized_);

    initialized = (initialized && (current_velocity_initialized_ && costmap_initialized_));
  }

  return initialized;
}

bool AstarAvoid::planAvoidWaypoints(int& end_of_avoid_index)
{

  bool found_path = false;
  int closest_waypoint_index = getLocalClosestWaypoint(avoid_waypoints_, current_pose_global_.pose, closest_search_size_);

  // update goal pose incrementally and execute A* search
  for (int i = search_waypoints_delta_; i < static_cast<int>(search_waypoints_size_); i += search_waypoints_delta_)
  {
    // update goal index
    int goal_waypoint_index = closest_waypoint_index + obstacle_waypoint_index_ + i;

    if (goal_waypoint_index >= static_cast<int>(avoid_waypoints_.waypoints.size()))
    {
      break;
    }

    ROS_INFO_STREAM("goal_pos_index: " << goal_waypoint_index);
    //update goal pose
    goal_pose_global_ = avoid_waypoints_.waypoints[goal_waypoint_index].pose;

    // frame "base_link" --> frame "base_footprint"
    goal_pose_local_.header = costmap_.header;
    goal_pose_local_.pose = transformPose(goal_pose_global_.pose,
                                          getTransform(costmap_.header.frame_id, goal_pose_global_.header.frame_id));


    // initialize costmap for A* search
    astar_.initialize(costmap_);
    astar_.init_param(use_back_,
                      use_potential_heuristic_,
                      use_wavefront_heuristic_,
                      time_limit_,
                      robot_length_,
                      robot_width_,
                      robot_base2back_,
                      minimum_turning_radius_,
                      theta_size_,
                      curve_weight_,
                      reverse_weight_,
                      lateral_goal_range_,
                      longitudinal_goal_range_,
                      angle_goal_range_,
                      obstacle_threshold_,
                      potential_weight_,
                      distance_heuristic_weight_);

    bool is_modified = astar_.modifyNodes(current_pose_local_.pose);

    ROS_INFO_STREAM("current_pose_global_ x: " << current_pose_global_.pose.position.x);
    ROS_INFO_STREAM("current_pose_global_ y: " << current_pose_global_.pose.position.y);
    ROS_INFO_STREAM("current_pose_global_orient x: " << current_pose_global_.pose.orientation.x);
    ROS_INFO_STREAM("current_pose_global_orient y: " << current_pose_global_.pose.orientation.y);
    ROS_INFO_STREAM("current_pose_global_orient z: " << current_pose_global_.pose.orientation.z);
    ROS_INFO_STREAM("current_pose_global_orient w: " << current_pose_global_.pose.orientation.w);

    ROS_INFO_STREAM("current_pose_local_ x: " << current_pose_local_.pose.position.x);
    ROS_INFO_STREAM("current_pose_local_ y: " << current_pose_local_.pose.position.y);
    ROS_INFO_STREAM("current_pose_local_orient x: " << current_pose_local_.pose.orientation.x);
    ROS_INFO_STREAM("current_pose_local_orient y: " << current_pose_local_.pose.orientation.y);
    ROS_INFO_STREAM("current_pose_local_orient z: " << current_pose_local_.pose.orientation.z);
    ROS_INFO_STREAM("current_pose_local_orient w: " << current_pose_local_.pose.orientation.w);

    // ROS_INFO_STREAM("goal_pose_local_ x: " << goal_pose_local_.pose.position.x);
    // ROS_INFO_STREAM("goal_pose_local_ y: " << goal_pose_local_.pose.position.y);

    // execute astar search
    // ros::WallTime start = ros::WallTime::now();

    if (is_modified) {
      found_path = astar_.makePlan(current_pose_local_.pose, goal_pose_local_.pose);
    }

    // ros::WallTime end = ros::WallTime::now();

    static ros::Publisher pub = nh_.advertise<nav_msgs::Path>("debug", 1);

    // ROS_INFO("Astar planning: %f [s], at index = %d", (end - start).toSec(), goal_waypoint_index);

    if (found_path)
    {
      pub.publish(astar_.getPath());
      end_of_avoid_index = goal_waypoint_index;
      mergeAvoidWaypoints(astar_.getPath(), end_of_avoid_index);
      if (avoid_waypoints_.waypoints.size() > 0)
      {
        ROS_INFO("Found GOAL at index = %d", goal_waypoint_index);
        astar_.reset();
        return true;
      }
      else
      {
        found_path = false;
      }
    }
    astar_.reset();
  }

  ROS_ERROR("Can't find goal...");
  return false;
}

void AstarAvoid::mergeAvoidWaypoints(const nav_msgs::Path& path, int& end_of_avoid_index)
{
  custom_msgs::Lane current_waypoints = avoid_waypoints_;

  // reset
  std::lock_guard<std::mutex> lock(mutex_);

  avoid_waypoints_.waypoints.clear();

  // add waypoints before start index
  int closest_waypoint_index = getLocalClosestWaypoint(current_waypoints, current_pose_global_.pose, closest_search_size_);
  for (int i = 0; i < closest_waypoint_index; ++i)
  {
    avoid_waypoints_.waypoints.push_back(current_waypoints.waypoints.at(i));
  }

  // set waypoints for avoiding
  for (const auto& pose : path.poses)
  {
    custom_msgs::Waypoint wp;
    wp.pose.header = avoid_waypoints_.header;
    wp.pose.pose = transformPose(pose.pose, getTransform(avoid_waypoints_.header.frame_id, pose.header.frame_id));
    wp.pose.pose.position.z = current_pose_global_.pose.position.z;  // height = const
    wp.twist.twist.linear.x = avoid_waypoints_velocity_ / 3.6;       // velocity = const
    avoid_waypoints_.waypoints.push_back(wp);
  }

  // add waypoints after goal index
  for (int i = end_of_avoid_index; i < static_cast<int>(current_waypoints.waypoints.size()); ++i)
  {
    avoid_waypoints_.waypoints.push_back(current_waypoints.waypoints.at(i));
  }

  // update index for merged waypoints
  end_of_avoid_index = closest_waypoint_index + path.poses.size();
}

void AstarAvoid::publishLocalPath(const custom_msgs::Lane& lane)
{
  nav_msgs::Path local_path;

  local_path.header.stamp = lane.header.stamp;
  local_path.header.frame_id = lane.header.frame_id;

  for (int i = 0; i < lane.waypoints.size(); ++i){
    
    geometry_msgs::PoseStamped ps;
    
    ps.header.stamp = lane.header.stamp;
    ps.header.frame_id = lane.header.frame_id;
    local_path.poses.push_back(lane.waypoints.at(i).pose);
  }

  local_path_pub_.publish(local_path);
}

void AstarAvoid::publishWaypoints()
{
  custom_msgs::Lane current_waypoints;

  while (!terminate_thread_)
  {
    // select waypoints
    switch (state_)
    {
      case AstarAvoid::STATE::RELAYING:
        current_waypoints = base_waypoints_;
        break;
      case AstarAvoid::STATE::STOPPING:
        // do nothing, keep current waypoints
        break;
      case AstarAvoid::STATE::PLANNING:
        // do nothing, keep current waypoints
        break;
      case AstarAvoid::STATE::AVOIDING:
        current_waypoints = avoid_waypoints_;
        break;
      default:
        current_waypoints = base_waypoints_;
        break;
    }

    custom_msgs::Lane safety_waypoints;
    safety_waypoints.header = current_waypoints.header;
    safety_waypoints.increment = current_waypoints.increment;

    // safety_waypoints.waypoints = current_waypoints.waypoints;

    // // push waypoints from closest index
    for (int i = 0; i < safety_waypoints_size_; ++i)
    {
      int index = getLocalClosestWaypoint(current_waypoints, current_pose_global_.pose, closest_search_size_) + i;

      // int index = 1 + i;

      if (index < 0 || static_cast<int>(current_waypoints.waypoints.size()) <= index)
      {
        break;
      }

      const custom_msgs::Waypoint& wp = current_waypoints.waypoints[index];
      safety_waypoints.waypoints.push_back(wp);
    }

    if (safety_waypoints.waypoints.size() > 0)
    {
      safety_waypoints_pub_.publish(safety_waypoints);
    }

    publishLocalPath(safety_waypoints);
    // ROS_INFO("Local path published");
    rate_->sleep();
  }
}

tf::Transform AstarAvoid::getTransform(const std::string& from, const std::string& to)
{
  tf::StampedTransform stf;
  try
  {
    tf_listener_.lookupTransform(from, to, ros::Time(0), stf);
  }
  catch (tf::TransformException ex)
  {
    ROS_ERROR("%s", ex.what());
  }
  return stf;
}

int AstarAvoid::getLocalClosestWaypoint(const custom_msgs::Lane& waypoints, const geometry_msgs::Pose& pose, const int& search_size)
{
  static custom_msgs::Lane local_waypoints;  // around self-vehicle
  const int prev_index = closest_local_index_;

  // search in all waypoints if lane_select judges you're not on waypoints

  if (closest_local_index_ == -1)
  {
    closest_local_index_ = getClosestWaypoint(waypoints, pose);
  }
  // search in limited area based on prev_index
  else
  {
    // get neighborhood waypoints around prev_index
    int start_index = std::max(0, prev_index - search_size / 2);
    int end_index = std::min(prev_index + search_size / 2, (int)waypoints.waypoints.size());
    // ROS_INFO_STREAM("start_index " << start_index);
    // ROS_INFO_STREAM("end_index " << end_index);


    auto start_itr = waypoints.waypoints.begin() + start_index;
    auto end_itr = waypoints.waypoints.begin() + end_index;
    local_waypoints.waypoints = std::vector<custom_msgs::Waypoint>(start_itr, end_itr);

    // ROS_INFO_STREAM("length " << local_waypoints.waypoints.size());

    // get closest waypoint in neighborhood waypoints
    closest_local_index_ = start_index + getClosestWaypoint(local_waypoints, pose);
    // ROS_INFO_STREAM("closest_local_index_ " << closest_local_index_);
  }

  return closest_local_index_;
}
