
#include "map_gen/map_gen.h"


// void currentPoseCallback(const geometry_msgs::PoseStamped& msg)
// {

//   tf::Transform transform;

//   geometry_msgs::PoseStamped current_pose_global_ = msg;

//   float pos_x = current_pose_global_.pose.position.x;
//   float pos_y = current_pose_global_.pose.position.y;
//   float quat_x = current_pose_global_.pose.orientation.x;
//   float quat_y = current_pose_global_.pose.orientation.y;
//   float quat_z = current_pose_global_.pose.orientation.z;
//   float quat_w = current_pose_global_.pose.orientation.w;

//   transform.setOrigin(tf::Vector3(pos_x, pos_y, 0.0));
//   transform.setRotation(tf::Quaternion(quat_x, quat_y, quat_z, quat_w));
//   br->sendTransform(tf::StampedTransform(transform, ros::Time::now(), "world", "base_link"));

// }


int main(int argc, char** argv)
{
  ros::init(argc, argv, "map_gen");
  
  // br = new tf::TransformBroadcaster();

  ros::NodeHandle nh;

  static ros::Publisher pub_global_cmap_ = nh.advertise<nav_msgs::OccupancyGrid>("world", 1, true);
  // ros::Subscriber current_pose_sub_ = nh.subscribe("current_pose", 1, currentPoseCallback);
	ros::Rate loop_rate(10);

  MapGen node;

	while(ros::ok()){

    pub_global_cmap_.publish(node.getGlobalMap());
    
   	ros::spinOnce();
   	
   	loop_rate.sleep();
  }

  
  return 0;
}