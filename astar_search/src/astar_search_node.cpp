
#include "astar_search/astar_search.h"

using namespace std;

int main(int argc, char** argv)
{
  ros::init(argc, argv, "astar_search");
  AstarSearch node;
  node.run();
  return 0;
}

