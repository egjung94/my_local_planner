## Supplementary Information

1. Autoware User Manual
* has rqt_graph like information about the code
https://github.com/CPFL/Autoware-Manuals/blob/master/en/Autoware_UsersManual_v1.1.md


## Prerequisites
sudo apt-get install ros-$ROS_DISTRO-grid-map

## Commands in order
```sh
alias no1_loadmap="rosrun map_gen map_gen_node"
alias no2_generateCostmap="roslaunch ~/catkin_ws/src/my_local_planner/costmap_generator/launch/costmap_generator.launch replay:=true"
alias no3_global_path="rosrun rospy_message_converter path_publisher.py"
alias no4_astar_avoid="roslaunch ~/catkin_ws/src/my_local_planner/waypoint_planner/launch/astar_avoid.launch replay:=true"
alias no5_velocity_set="roslaunch ~/catkin_ws/src/my_local_planner/waypoint_planner/launch/velocity_set.launch replay:=true"
```

## add the following line to os1.launch
```html
  <node pkg="tf2_ros" type="static_transform_publisher" name="local_to_ouster_broadcaster" args="1.5 0.0 0 0 0 0 1.0 base_footprint os1_sensor" />
```

## Gitlab 쓰는 법 

https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html

https://stackabuse.com/git-merge-branch-into-master/

```sh
$ git branch #pwd-like function; prints the current working branch
```

Below is the working process
```sh

$ git pull #START YOUR WORK #get original file on GITLAB

$ git checkout master #make sure you are on the master branch

#Case1: Access new branch 
$ git branch <NEW-BRANCH>
$ git checkout -b <name-of-branch> #make a new branch

#Case2: Access a previosly made branch
$ git checkout <PREVIOSULY-MADE-BRANCH>

# ...develop some code...
	# ...REPEAT
$ git add –A

$ git commit –m "Some commit message"
	# ...REPEAT

# Merge to local master branch
$ git checkout master

$ git merge new-branch

#Case1:PUSH CURRENT BRANCH TO GITLAB
$ git push 
#Case2:PUSH NEW BRANCH TO GITLAB
$ git push --set-upstream origin <NEW-Branch>

```


